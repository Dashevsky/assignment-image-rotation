#ifndef ROTATE_H
#define ROTATE_H

struct image rotate( struct image const* old_image );

#endif /*ROTATE_H*/
