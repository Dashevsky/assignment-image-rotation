#ifndef BMP_H
#define BMP_H

#include <stdio.h>
#include <stdint.h>

#include "image.h"



enum read_status  {
	READ_SUCCESS = 0,
	READ_PIXEL_ERROR,
	READ_HEADER_ERROR,
	READ_INVALID_SIGNATURE,
	READ_INVALID_BITS
};


enum write_status  {
	WRITE_SUCCESS = 0,
	WRITE_ERROR
};

enum header_status {
	HEADER_BFTYPE_ERROR = 0,
	HEADER_BIBITCOUNT_ERROR,
	HEADER_VALID
};

enum read_status read_from_bmp( FILE* input_file, struct image* image );
enum write_status write_to_bmp( FILE* output_file, struct image const* image );

#endif /*BMP_H*/
