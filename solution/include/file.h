#ifndef FILE_H
#define FILE_H

#include <stdio.h>
#include <stdint.h>


enum open_to_read_status {
	OPEN_TR_SUCCESS = 0,
	OPEN_CONTENT_ERROR,
	OPEN_FILENAME_ERROR
};

enum open_to_write_status {
	OPEN_TW_SUCCESS = 0
};



enum open_to_read_status open_file_to_read(char const* name, FILE** input_file);
enum open_to_write_status open_file_to_write(char const* name, FILE** output_file);



#endif /*FILE_H*/

