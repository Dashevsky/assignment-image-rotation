#ifndef IMAGE_H
#define IMAGE_H

#include <stdio.h>
#include <stdint.h>

struct __attribute__((packed)) pixel 
{ 
	uint8_t b, g, r; 
};

struct image
{
	uint64_t width, height;
	struct pixel* data;
};

struct image create_image(uint64_t, uint64_t);
void delete_image(struct image *);

#endif /*IMAGE_H*/
