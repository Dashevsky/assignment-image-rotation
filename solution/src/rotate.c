#include "rotate.h"

#include <stdlib.h>

#include "image.h"

static struct pixel* get_pixel(struct pixel* image, const uint64_t w , const size_t j, const size_t i) {
	return &image[w * i + j];
} 

// поворот на 90 градусов
struct image rotate( struct image const* old_image )
{
	// создаем новую картинку поменяв местами размеры старой
	struct image new_image = create_image(old_image->height, (*old_image).width);

	// поворачиваем картинку, переставляя пиксели по очереди
	for (size_t i = 0; i < old_image->height; i++)
		for (size_t j = 0; j < old_image->width; j++)
			*get_pixel(new_image.data, old_image->height, old_image->height - i - 1, j) = *get_pixel(old_image->data, old_image->width, j, i);

	return new_image;

}
