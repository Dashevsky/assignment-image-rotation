#include "file.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

enum open_to_read_status open_file_to_read(char const* name, FILE** input_file)
{
	// проверяем возможность доступа к файлу и открываем его на чтение
	if (access( name, F_OK ) != 0) { return OPEN_FILENAME_ERROR; }
	*input_file = fopen(name, "rb");

	// проверяем, нормально ли с ним все
	if (input_file == NULL) { return OPEN_CONTENT_ERROR; } //тут файл при ошибке закрывать не надо
	return OPEN_TR_SUCCESS;
}


enum open_to_write_status open_file_to_write(char const *name, FILE** output_file)
{
	// открываем на запись
	// я не придумал как оно может сломаться (там же все равно либо перезапишется старый, либо создастся новый?)
	*output_file = fopen(name, "wb");
	return OPEN_TW_SUCCESS;
}

