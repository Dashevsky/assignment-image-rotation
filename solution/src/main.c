#include <stdlib.h>

#include "file.h"
#include "bmp.h"
#include "image.h"
#include "rotate.h"


int main( int argc, char** argv )
{
	char* input_name;
	char* output_name;

	FILE* input_file;
	FILE* output_file;


	// если аргументов два (есть только имя первой картинки), то я просто называю выходную rotated-picture.bmp, так было быстрее тестировать
	// если не три и не два, то ошибка
	if (argc == 3)
		output_name = argv[2];
	else if (argc == 2)
		output_name = "rotated-picture.bmp";
	else
	{ fprintf(stderr, "Неверно указаны аргументы\n"); return 1; }

	input_name = argv[1];



	// открываем файл для чтения и реагируем на ошибки
	enum open_to_read_status os = open_file_to_read(input_name, &input_file); // передаю в функции работы с файлами указатель на FILE*, потому что FILE* еще не инициализирован
	switch (os)
	{
		case OPEN_FILENAME_ERROR: { fprintf(stderr, "Ошибка в имени входного файла\n"); return 1; }
		case OPEN_CONTENT_ERROR:{ fprintf(stderr, "Ошибка содержимого входного файла\n"); return 1; }
		case OPEN_TR_SUCCESS: { fprintf(stderr,"Входной файл считан\n"); break; }
	}

	// объявляем картинку и читаем ее из открытого файла
	struct image old_img;
	enum read_status rs = read_from_bmp(input_file, &old_img); // тут уже можно передавать просто FILE*

	if(fclose(input_file)) // не забываем закрыть файл и реагируем на ошибки (в том числе при закрытии файла)
	{
		fprintf(stderr, "Ошибка закрытия входного файла\n"); return 1;
	}	
	switch (rs)
	{
		case READ_PIXEL_ERROR: { fprintf(stderr, "Ошибка чтения пикселей входного файла\n"); return 1; }
		case READ_HEADER_ERROR: { fprintf(stderr, "Ошибка чтения заголовка входного файла\n"); return 1; }
		case READ_INVALID_SIGNATURE: { fprintf(stderr, "Заголовок поврежден. Недопустимый тип файла.\n"); return 1; }
		case READ_INVALID_BITS: { fprintf(stderr, "Заголовок поврежден. Неверная битность.\n"); return 1; } 
		case READ_SUCCESS: { fprintf(stderr,"Содержимое входного файла считано\n"); break; }
	}



	// объявляем новую картинку и сразу же записываем туда старую перевернутую
	struct image new_img = rotate(&old_img);
	delete_image(&old_img); // не забываем удалить старую картинку из памяти
	fprintf(stderr,"Изображение повернуто\n");



	// открываем файл для записи и реагируем на ошибки, которые я не придумал (там же все равно либо перезапишется старый, либо создастся новый?)
	enum open_to_write_status cs = open_file_to_write(output_name, &output_file);
	switch (cs)
	{
		case OPEN_TW_SUCCESS: { fprintf(stderr,"Файл для записи открыт\n"); break; }
	}

	// записываем картинку в открытый файл и реагируем на ошибки
	enum write_status ws = write_to_bmp(output_file, &new_img);
	delete_image(&new_img); // не забываем удалить новую картинку из памяти
	switch (ws)
	{
		case WRITE_ERROR: { fprintf(stderr, "Ошибка записи\n");	 return 1; }
		case WRITE_SUCCESS: { fprintf(stderr,"Записано успешно!\n"); break; }
	}

	if(fclose(output_file))	// закрываем выходной файл
	{
		fprintf(stderr, "Ошибка закрытия выходного файла\n"); return 1;
	}	
	
	printf("Готово!");
	return 0;
}
