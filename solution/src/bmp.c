#include "bmp.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "image.h"

#define BFTYPE 0x4d42
#define BFRESERVED 0
#define BISIZE 40
#define BIPLANES 1
#define BIBITCOUNT 24
#define BICOMPRESSION 0
#define BIXPELSPERMETER 0
#define BIYPELSPERMETER 0
#define BICLRUSED 0
#define BICLRIMPORTANT 0


struct __attribute__((packed)) bmp_header
{
	uint16_t bfType;
	uint32_t bfileSize;
	uint32_t bfReserved;
	uint32_t bOffBits;
	uint32_t biSize;
	uint32_t biWidth;
	uint32_t biHeight;
	uint16_t biPlanes;
	uint16_t biBitCount;
	uint32_t biCompression;
	uint32_t biSizeImage;
	uint32_t biXPelsPerMeter;
	uint32_t biYPelsPerMeter;
	uint32_t biClrUsed;
	uint32_t biClrImportant;
};

static struct bmp_header header_set(const struct image* image)
{
	struct bmp_header header = {0};
	header.bfType = BFTYPE;
	header.bfileSize = (*image).height * ((((*image).width + 4-1)/4)*4) * sizeof(struct pixel) + sizeof(struct bmp_header);
	header.bfReserved = BFRESERVED;
	header.bOffBits = sizeof(struct bmp_header);
	header.biSize = BISIZE;
	header.biWidth = (*image).width;;
	header.biHeight = (*image).height;
	header.biPlanes = BIPLANES;
	header.biBitCount = BIBITCOUNT;
	header.biCompression = BICOMPRESSION;
	header.biSizeImage = (*image).height * ((((*image).width + 4-1)/4)*4) * sizeof(struct pixel);
	header.biXPelsPerMeter = BIXPELSPERMETER;
	header.biYPelsPerMeter = BIYPELSPERMETER;
	header.biClrUsed = BICLRUSED;
	header.biClrImportant = BICLRIMPORTANT;
	return header;
}


enum header_status check_header(struct bmp_header header)
{
	if (header.bfType != BFTYPE)
		return HEADER_BFTYPE_ERROR;

	if (header.biBitCount != BIBITCOUNT)
		return HEADER_BIBITCOUNT_ERROR;
	
	return HEADER_VALID;
}


static uint32_t padding(uint32_t w)
{
	return w % 4;
}


static struct pixel* get_row_addr(size_t i, const struct image* image) {
	return &(image->data)[i * (image->width)];
}


enum read_status read_from_bmp( FILE* input_file, struct image* image )
{
	// инициализируем заголовок и записываем его из файла
	// fread(ссылка на структуру, размер заголовка, колво(один), указатель на FILE*)
	// если считали не один элемент (один наш заголовок), то ошибка чтения заголовка
	struct bmp_header header = {0};
	if (fread(&header, sizeof(struct bmp_header), 1, input_file)!= 1) { return READ_HEADER_ERROR; }
	
	switch (check_header(header))
	{
		case HEADER_BFTYPE_ERROR: { return READ_INVALID_SIGNATURE; }
		case HEADER_BIBITCOUNT_ERROR: { return READ_INVALID_BITS; }
		case HEADER_VALID: {};
	}

	
	// создаем картинку по полученным из заголовка данным
	(*image) = create_image((header).biWidth, (header).biHeight);

	// считываем строки по одной
	for (size_t i = 0; i < image->height; i++)
	{
	    // читаем из файла строку пикселей
	    // fread(указатель на байты первого пикселя в строке массиве картинки, размер пикселя, их количество (ширина картинки), указатель на FILE*)
	    // если считали меньше пикселей чем ширина картинки, то ошибка чтения пикселей
		if (fread(get_row_addr(i, image), sizeof(struct pixel), image->width, input_file) != image->width) return READ_PIXEL_ERROR;
		// смещаем в файле указатель положения на размер паддинга от текущей позиции (SEEK_CUR), чтобы перейти к следующей строчке пикселей
		if (fseek(input_file, padding((header).biWidth), SEEK_CUR)) return READ_PIXEL_ERROR;
	}

	return READ_SUCCESS;
}


enum write_status write_to_bmp( FILE* output_file, struct image const* image )
{
	// создаем заголовок
	struct bmp_header header = header_set(image);

	// пишем в файл фаголовок
	// fwrite(указатель на заголовок, его размер, количество (1), указатель на FILE*)
	// если записан не один элемент, то ошибка записи
	if (fwrite(&header, sizeof(struct bmp_header), 1, output_file) != 1) { return WRITE_ERROR; }

	// задаем мусорное значение в один пиксель чтобы забивать паддинг и начинаем перебирать строки для записи по одной
	uint32_t const junk_value = 0;
	for (size_t i = 0; i < image->height; i++)
	{
	    // пишем строку пикселей
	    // fwrite(указатель на байты пикселя (начиная с которого мы пишем всю строчку в массив), размер пикселя, колво пикселей (ширина), указатель на FILE*)
	    // если записали не столько пикселей, сколько ширина картинки, то ошибка записи
		if (fwrite(get_row_addr(i, image), sizeof(struct pixel), image->width, output_file) != (header).biWidth) return WRITE_ERROR;
		// пишем отступ (паддинг) после каждой строки
		// fwrite(ссылка на мусорное значение, (один элемент), размер паддинга (столько нам надо записать мусора), указатель на FILE*)
		// если записали не столько мусора, сколько паддинг, то ошибка записи
		if (fwrite(&junk_value, 1, padding((header).biWidth), output_file) != padding((header).biWidth)) return WRITE_ERROR;
	}

	return WRITE_SUCCESS;
}




