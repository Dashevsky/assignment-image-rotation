#include "image.h"

#include <stdlib.h>

struct image;

// создание картинки
struct image create_image(uint64_t w, uint64_t h)
{
    return (struct image) { .width = w, .height = h, .data = malloc(w * h * sizeof(struct pixel))};
}


// освобождение того, под что мы в картинке аллоцировали память
void delete_image(struct image* i)
{
    free(i->data);
}
